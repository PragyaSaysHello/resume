
// var ul = document.getElementById('list'),
var removeAll = document.getElementById('removeAll'),
    add = document.getElementById('add');
     

add.onclick = function() {
    addLi(myUL);
}

function addLi(targetUI) {
    var inputText = document.getElementById('input').value,
        li = document.createElement('li'),
        textNode = document.createTextNode(inputText + " ")
        removeButton = document.createElement("button");
        document.getElementById("input").value = '';
   

    if (inputText.length === 0) {
        alert("You havenot entered anything!");
        return false;
    }
    removeButton.className = 'removeMe';
   
    removeButton.innerHTML='<i class="fa fa-trash-o"></i>';

    removeButton.setAttribute("onclick", "removeMe(this);");


    li.appendChild(textNode);
    li.appendChild(removeButton);

    targetUI.appendChild(li);

    
}

function removeMe(item) {
    var parent = item.parentElement;
    parent.parentElement.removeChild(parent);
}
removeAll.onclick = function() {
   myUL.innerHTML = "";
}

